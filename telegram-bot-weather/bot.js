const { Telegraf } = require('telegraf');
const axios = require('axios');
const bot = new Telegraf('6091486565:AAGxPgcRoikAulfvCpM-mqUL1Hy1B4kmB7k');
bot.start((ctx) => ctx.reply('send me your geolocation'));
bot.on('message', async (ctx)=>{
    if(ctx.message.location){
        console.log(ctx.message.location);
        const url = `https://api.openweathermap.org/data/2.5/weather?lat=${ctx.message.location.latitude}&lon=${ctx.message.location.longitude}&appid=6afe7a5ea1d1a9e8646d1baa9e009018`;
    const response = await axios.get(url);
    console.log(response);
    response.data.main.temp = response.data.main.temp -273.15;
    temperature = response.data.main.temp;
    ctx.reply(`${response.data.name}: ${temperature.toFixed(1)} C`);

    }
    
});
bot.launch();

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));