//Добавляем прослушку на всем окне
//target -элемент,по которому мы кликнули
window.addEventListener(('click'), function (event) {
    //Объявляем переменную для счетчика
    let counter;
    //Проверяем клик строго по кнопка плюс либо минус
    if (event.target.dataset.action === 'plus' || event.target.dataset.action === 'minus'){
        console.log(true);
        //Находим обертку счетчика
    const counterWrapper = event.target.closest('.counter-wrapper');
    //поиск внутри родителя
    counter = counterWrapper.querySelector('[data-counter]');
    }
    
    //Проверяем является ли элемент,по которому совершили клик кнопкой плюс
    if (event.target.dataset.action === 'plus'){
    //Находим обертку счетчика
    counter.innerText = ++counter.innerText;

}
    //Проверяем является ли элемент, по которому совершили клик кнопкой минус
    if (event.target.dataset.action === 'minus'){
     //Проверяем, чтобы счетчик был больше 1
    if (parseInt(counter.innerText) >1) {
        //Изменяем текст в счетчике, уменьшая его на 1.
    counter.innerText = --counter.innerText;
    }
    else if (event.target.closest('.cart-wrapper')&& parseInt(counter.innerText) ===1)
    {      // Проверка на товар, который находится в корзине
            console.log('IN CART!');
            //удаляем товар из корзины
            event.target.closest('.cart-item').remove();
            //Отображение статуса корзины Пустая/Полная
            toggleCartStatus();
            //Пересчет общей стоимости корзины Пустая/Полная
            calcCartPriceAndDelivery();
        

    }
    
    }
    //Проверяем клик на + или - внутри корзины
    if( event.target.hasAttribute('data-action')&& event.target.closest('.cart-wrapper')) {
        //Пересчет общей стоимости товаров в корзине
        calcCartPriceAndDelivery();
    }
});
