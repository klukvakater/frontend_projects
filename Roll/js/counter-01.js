
const btnMinus = document.querySelector('[data-action="minus"]');
const btnPlus = document.querySelector('[data-action="plus"]');
const counter = document.querySelector('[data-counter]');
// Находим во всем документы элементы - и + кнопки, и сам счетчик
//Отслеживаем клик по кнопкам(1 событие на странице-наведение, клик, на клавиатуре, у нас просто клик по кнопке)
btnMinus.addEventListener('click', function (){
    //Проверяем, чтобы счетчик был больше 1
    if (parseInt(counter.innerText) >1) {
        //Изменяем текст в счетчике, уменьшая его на 1.
    counter.innerText = --counter.innerText;
    }

});
// обращаемся к свойству, которое прописано внутри тега/ innerText- можно заменить на число, которое хочу
//здесь получаем значение, которое указано в элементе.
//сначала увеличиваем значение, затем сразу его возвращаем.
btnPlus.addEventListener('click', function (){
    
    counter.innerText = ++counter.innerText;
});
// увеличивать либо уменьшать клик счетчика
