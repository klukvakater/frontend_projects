
function calcCartPriceAndDelivery() {
const cartWrapper = document.querySelector('.cart-wrapper');
const priceElememts =cartWrapper.querySelectorAll('.price__currency');
const totalpriceEl =document.querySelector('.total-price');
const deliveryCost = document.querySelector('.delivery-cost');
const cartDelivery = document.querySelector('[data-cart-delivery]');
let totalPrice = 0;
//Обходим все блоки с ценами в корзине
priceElememts.forEach(function(item) {
    //Находим количество товара
    const amountEl =item.closest('.cart-item').querySelector('[data-counter]');
    //Добавляем стоимость товара в общую стоимость(кол-во* цену)
    totalPrice += parseInt(item.innerText)* parseInt(amountEl.innerText);
});
//Отображаем цену на странице
totalpriceEl.innerText = totalPrice;
if (totalPrice >0)
{   cartDelivery.classList.remove('none');
}else {
    cartDelivery.classList.add('none');
}
if (totalPrice >=600) {
    deliveryCost.classList.add('free');
    deliveryCost.innerText = 'бесплатно';
 } else {
    deliveryCost.classList.remove('free');
    deliveryCost.innerText = '250 Р';
    }
}
